/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.father1;

/**
 *
 * @author MSI GAMING
 */
public class Mainprogram { // สร้าง class Mainprogram
    public static void main(String[] args) { 
        ABC abc = new ABC ("ABC",20,"female",60,175.5); //abc ถูกสร้างขึ้น ( name , age , sex, weight , height)
        XYZ xyz = new XYZ("XYZ",23,"man",100,175.0);//xyz ถูกสร้างขึ้น ( name , age , sex, weight , height)
        
        abc.setBMI(40, 160); // abc เรียกใช้ method setBMI (40 คือ น้ำหนัก , 160 คือ ส่วนสูง)
        abc.setBMI("ABC", 40, 160); // abc เรียกใช้ method setBMI (ABC คือชื่อ ,40 คือ น้ำหนัก , 160 คือ ส่วนสูง) จะเห็นว่า method setBMI รับพารามิเตอร์ไม่เหมือนกันกับข้างบน แต่ชื่อซ้ำ นี่แหละคือ Overload
        abc.setBMI(60, 175.5); // abc เรียกใช้ method setBMI เปลี่ยน น้ำหนักเป็น 60 ส่วนสูงเป็น 175.5
        abc.setBMI("ABC", 60, 175.5); // Overload method setBMI เปลี่ยน น้ำหนักเป็น 60 ส่วนสูงเป็น 175.5 และเพิ่มพารามิเตอร์ name
        abc.setBMI(80, 175.5); // abc เรียกใช้ method setBMI เปลี่ยน น้ำหนักเป็น 80 ส่วนสูงเป็น 175.5
        abc.setBMI("ABC", 80, 175.5);  // Overload method setBMI เปลี่ยน น้ำหนักเป็น 80 ส่วนสูงเป็น 175.5 และเพิ่มพารามิเตอร์ name
        System.out.println("------------------------------------------------------------------------");
        xyz.setBMI(100, 170); // xyz เรียกใช้ method setBMI (100 คือ น้ำหนัก , 170 คือ ส่วนสูง)
        xyz.setBMI(23, 100, 175);  // xyz เรียกใช้ method setBMI (23 คือ อายุ ,100 คือ น้ำหนัก , 170 คือ ส่วนสูง) จะเห็นว่า method setBMI รับพารามิเตอร์ไม่เหมือนกันกับข้างบน แต่ชื่อซ้ำ นี่แหละคือ Overload
        xyz.setBMI(40, 160); // xyz เรียกใช้ method setBMI เปลี่ยน น้ำหนักเป็น 40 ส่วนสูงเป็น 160
        xyz.setBMI(15, 40, 160);// Overload method setBMI เปลี่ยน น้ำหนักเป็น 40 ส่วนสูงเป็น 160 และเพิ่มพารามิเตอร์ age 
        xyz.setBMI(60, 175.5);// xyz เรียกใช้ method setBMI เปลี่ยน น้ำหนักเป็น 60 ส่วนสูงเป็น 175.5
        xyz.setBMI(50, 60, 175.5);// Overload method setBMI เปลี่ยน น้ำหนักเป็น 60 ส่วนสูงเป็น 175.5 และเพิ่มพารามิเตอร์ age
        
    }
}
