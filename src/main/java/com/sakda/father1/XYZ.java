/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.father1;

/**
 *
 * @author MSI GAMING
 */
public class XYZ extends father{

    public XYZ(String name, int age, String sex, double weight, double height) {
        super(name, age, sex, weight, height);
    }
    @Override
    public void setBMI(double weight, double height) {// จะเห็นว่า method setBMI ชื่อและพารามิเตอร์ เหมือนกันกับ class แม่ นี่แหละคือ Override
        this.weight = weight;
        this.height = height;
        double height1; // สร้างตัวแปร height1 ชนิด double ให้เก็บค่า ส่วนสูงหน่วยเป็นเมตร
        height1 = height / 100; // แปลงหน่วยส่วนสูงจากเซนติเมตร เป็นเมตร ด้วยการ หาร100
        BMI = (weight / (height1 * height1));  // ( นำหนัก / (  ส่วนสูง(เมตร)*ส่วนสูง(เมตร)  )  )
        if (BMI < 18.5) { // ถ้า BMI น้อยกว่า 18.5
            System.out.printf("Test Overriding A " + "BMI = %.2f  underweight  ", BMI); // แสดงผล ค่า BMI ทศนิยม2ตำแหน่ง และ น้ำหนักต่ำกว่าเกณฑ์
            System.out.println("");
        } else if (BMI < 23.0) {  // ถ้า BMI น้อยกว่า 23.0
            System.out.printf("Test Overriding A " + "BMI = %.2f  slim  ", BMI); // แสดงผล ค่า BMI ทศนิยม2ตำแหน่ง และ สมส่วน
            System.out.println("");
        } else if (BMI < 25.0) { // ถ้า BMI น้อยกว่า 25.0
            System.out.printf("Test Overriding A " + "BMI = %.2f  overweight", BMI); // แสดงผล ค่า BMI ทศนิยม2ตำแหน่ง และ น้ำหนักเกิน
            System.out.println("");
        } else if (BMI < 30.0) { // ถ้า BMI น้อยกว่า 30.0
            System.out.printf("Test Overriding A " + "BMI = %.2f   obesity", BMI); // แสดงผล ค่า BMI ทศนิยม2ตำแหน่ง และ โรคอ้วน
            System.out.println("");
        } else { // ถ้าไม่ตรงเงื่อนไขใดเลย
            System.out.printf("Test Overriding A " + "BMI = %.2f  !!! Dangerous obesity !!!", BMI);  // แสดงผล ค่า BMI ทศนิยม2ตำแหน่ง และ โรคอ้วนอันตราย
            System.out.println("");
        }
    }
    //Overload
    public void setBMI(int age,double weight,double height){
       this.age = age;
        this.weight = weight;
        this.height = height;
        double height1;
        height1 = height/100;
        BMI = (weight / (height1*height1));
        if(  BMI < 18.5){
            System.out.printf("Test Overload"+" Age ="+ age +"years old  BMI = %.2f  underweight  " ,BMI);
            System.out.println("");
        }else if (BMI< 23.0){
            System.out.printf("Test Overload"+" Age ="+ age +"years old BMI = %.2f  slim  ",BMI);
            System.out.println("");
        }else if(BMI < 25.0){
            System.out.printf("Test Overload"+" Age ="+ age +"years old BMI = %.2f  overweight",BMI);
            System.out.println(""); 
        }else if(BMI < 30.0){
            System.out.printf("Test Overload"+" Age ="+ age +"years old BMI = %.2f   obesity",BMI);
            System.out.println("");
        }else{
            System.out.printf("Test Overload"+" Age ="+ age +"years old BMI = %.2f  !!! Dangerous obesity !!!",BMI);
            System.out.println("");
        }
    } 
    
}
