/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.father1;

/**
 *
 * @author MSI GAMING
 */
public class father { // สร้าง class father

    protected String name; // สร้างตัวแปร name ชนิด String เอาไว้เก็บชื่อ
    protected int age; // สร้างตัวแปร age ชนิด int เอาไว้เก็บอายุ
    protected String sex; // สร้างตัวแปร sex ชนิด String เอาไว้เก็บเพศ
    protected double weight; // สร้างตัวแปร weight ชนิด double เอาไว้เก็บน้ำหนัก
    protected double height; // สร้างตัวแปร height ชนิด double เอาไว้เก็บส่วนสูง
    protected double BMI; // สร้างตัวแปร weight ชนิด double เอาไว้เก็บค่า BMI

    public father(String name, int age, String sex, double weight, double height) { //สร้าง Constructor father
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.weight = weight;
        this.height = height;
    }

    public void setBMI(double weight, double height) {
        this.weight = weight;
        this.height = height;
        double height1; // สร้างตัวแปร height1 ชนิด double ให้เก็บค่า ส่วนสูงหน่วยเป็นเมตร
        height1 = height / 100; // แปลงหน่วยส่วนสูงจากเซนติเมตร เป็นเมตร ด้วยการ หาร100
        BMI = (weight / (height1 * height1)); // ( นำหนัก / (  ส่วนสูง(เมตร)*ส่วนสูง(เมตร)  )  )
        if (BMI < 18.5) { // ถ้า BMI น้อยกว่า 18.5
            System.out.printf("BMI = %.2f  underweight  ", BMI); // แสดงผล ค่า BMI ทศนิยม2ตำแหน่ง และ น้ำหนักต่ำกว่าเกณฑ์
            System.out.println("");
        }else if(BMI < 23.0){ // ถ้า BMI น้อยกว่า 23.0
            System.out.printf("BMI = %.2f  slim  ",BMI); // แสดงผล ค่า BMI ทศนิยม2ตำแหน่ง และ สมส่วน
            System.out.println("");
        }else if(BMI <25.0){// ถ้า BMI น้อยกว่า 25.0
            System.out.printf("BMI = %.2f  overweight",BMI); // แสดงผล ค่า BMI ทศนิยม2ตำแหน่ง และ น้ำหนักเกิน
            System.out.println("");
        }else if(BMI <30.0){ // ถ้า BMI น้อยกว่า 30.0
            System.out.printf("BMI = %.2f   obesity",BMI); // แสดงผล ค่า BMI ทศนิยม2ตำแหน่ง และ โรคอ้วน
            System.out.println("");
        }else{ // ถ้าไม่ตรงเงื่อนไขใดเลย
            System.out.printf("BMI = %.2f  !!! Dangerous obesity !!!",BMI); // แสดงผล ค่า BMI ทศนิยม2ตำแหน่ง และ โรคอ้วนอันตราย
            System.out.println("");
        }

    }
}